#!/bin/bash

cd ..
#
#usage="To run tests on localhost: $0 local orb|Koordinator|Starter|Monitor|rechne|liste|terminate \
	#	else:\
	#$0 remote Koordinator <Name> <ORBHost> <ORBPort>\
	#$0 remote Starter <Name> <Koordinator Name> <ORBHost> <ORBPort>\
	#$0 remote Monitor <Name> <ORBHost> <ORBPort>\
	#$0 remote rechne <Koordinator Name> <Monitor Name> <min. Prozesse> <max. Prozesse> <minDelay> <maxDelay> <exitDelay> <ggT> <ORBHost> <ORBPort>\
	#$0 remote liste <Koordinator Name> <ORBHost> <ORBPort>\
	#$0 remote terminate <Koordinator Name> <ORBHost> <ORBPort>"
usage='...!'

if [ "$1" == "local" ]; 
then
	
	if [ "$2" == "orb" ]
	then
		orbd -ORBInitialPort 1050
	elif [ "$2" == "Koordinator" ]; 
	then
		java -cp Koordinator/bin/ Koordinator.Main Koordinator -ORBInitialHost localhost -ORBInitialPort 1050
	elif [ "$2" == "Starter" ] 
	then
		java -cp .:Starter/bin/:Koordinator/bin/ Starter.Main Starter Koordinator localhost 1050 -ORBInitialHost localhost -ORBInitialPort 1050
		elif [ "$2" == "Starter2" ] 
	then
		java -cp .:Starter/bin/:Koordinator/bin/ Starter.Main Starter2 Koordinator localhost 1050 -ORBInitialHost localhost -ORBInitialPort 1050
		elif [ "$2" == "Starter3" ] 
	then
		java -cp .:Starter/bin/:Koordinator/bin/ Starter.Main Starter3 Koordinator localhost 1050 -ORBInitialHost localhost -ORBInitialPort 1050
	elif [ "$2" == "Monitor" ]; 
	then
		java -jar GGTMonitor.jar Monitor -ORBInitialHost localhost -ORBInitialPort 1050
	elif [ "$2" == "rechne" ]; 
	then
		java -cp Client/bin/:Koordinator/bin/ Client.Main rechne Koordinator Monitor 5 5 1000 1001 2000 997 -ORBInitialHost localhost -ORBInitialPort 1050
	elif [ "$2" == "rechne2" ]; 
	then
	java -cp Client/bin/:Koordinator/bin/ Client.Main rechne Koordinator Monitor 15 15 2000 5000 1000 997 -ORBInitialHost localhost -ORBInitialPort 1050
	elif [ "$2" == "rechne3" ]; 
	then
		java -cp Client/bin/:Koordinator/bin/ Client.Main rechne Koordinator Monitor 5 5 2000 5000 1000 81839 -ORBInitialHost localhost -ORBInitialPort 1050
	elif [ "$2" == "liste" ]; 
	then
		java -cp Client/bin/:Koordinator/bin/ Client.Main liste Koordinator -ORBInitialHost localhost -ORBInitialPort 1050
	elif [ "$2" == "terminate" ]; 
	then
		java -cp Client/bin/:Koordinator/bin/ Client.Main terminate Koordinator -ORBInitialHost localhost -ORBInitialPort 1050
	else
		echo "$usage"
	fi
	
elif [ "$1" == "remote" ]; 
then

	if [ "$2" == "orb" ]
	then
		orbd -ORBInitialPort $3
	elif [ "$2" == "Koordinator" ]; 
	then
		java -cp Koordinator/bin/ Koordinator.Main $3 -ORBInitialHost $4 -ORBInitialPort $5
	elif [ "$2" == "Starter" ] 
	then
		java -cp .:Starter/bin/:Koordinator/bin/ Starter.Main $3 $4 $5 $6 -ORBInitialHost $5 -ORBInitialPort $6
	elif [ "$2" == "Monitor" ]; 
	then
		java -jar GGTMonitor.jar $3 -ORBInitialHost $4 -ORBInitialPort $5
	elif [ "$2" == "rechne" ]; 
	then
		java -cp Client/bin/:Koordinator/bin/ Client.Main rechne $3 $4 $5 $6 $7 $8 $9 ${10} -ORBInitialHost ${11} -ORBInitialPort ${12}
	elif [ "$2" == "liste" ]; 
	then
		java -cp Client/bin/:Koordinator/bin/ Client.Main liste $3 -ORBInitialHost $4 -ORBInitialPort $5
	elif [ "$2" == "terminate" ]; 
	then
		java -cp Client/bin/:Koordinator/bin/ Client.Main terminate $3 -ORBInitialHost $4 -ORBInitialPort $5
	else
		echo "$usage"

	fi
else
	printf '%s' $usage
fi