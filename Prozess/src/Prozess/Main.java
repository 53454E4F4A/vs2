package Prozess;

import Distributed_GCD.Koordinator;
import Distributed_GCD.KoordinatorHelper;
import Distributed_GCD.Prozess;
import Distributed_GCD.ProzessHelper;

import org.omg.CORBA.ORB;
import org.omg.CosNaming.NamingContextExt;
import org.omg.CosNaming.NamingContextExtHelper;
import org.omg.PortableServer.POA;
import org.omg.PortableServer.POAHelper;

import java.util.Properties;

public class Main {

	public static void main(String args[]) {
		String starterName = args[0];
		int id = Integer.parseInt(args[1]);
		String koordinator = args[2];
		try {
			Properties p = new Properties();
			
			ORB orb = ORB.init(args, p);

			POA rootPoa = POAHelper.narrow(orb
					.resolve_initial_references("RootPOA"));
			rootPoa.the_POAManager().activate();
			NamingContextExt nc = NamingContextExtHelper.narrow(orb
					.resolve_initial_references("NameService"));
			org.omg.CORBA.Object obj = nc.resolve_str(koordinator);
			Koordinator k = KoordinatorHelper.narrow(obj);

			final ProzessImpl pr = new ProzessImpl(orb, rootPoa, starterName,
					id, k);
			org.omg.CORBA.Object ref = rootPoa.servant_to_reference(pr);
			Prozess href = ProzessHelper.narrow(ref);

			k.processHinzufuegen(href, starterName + id);
			Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {

				@Override
				public void run() {
					pr.terminate();

				}

			}));
			orb.run();
		
		} catch (Exception e) {
			System.err.println("ERROR: " + e);
			// e.printStackTrace(System.out);
		}

	}
}
