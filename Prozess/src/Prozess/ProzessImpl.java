package Prozess;


import Distributed_GCD.Koordinator;
import Distributed_GCD.Prozess;
import Distributed_GCD.ProzessPOA;
import monitor.Monitor;

import org.omg.CORBA.ORB;
import org.omg.PortableServer.POA;
import org.omg.PortableServer.POAPackage.ObjectNotActive;
import org.omg.PortableServer.POAPackage.ServantNotActive;
import org.omg.PortableServer.POAPackage.WrongPolicy;





public class ProzessImpl extends ProzessPOA {

	private Thread ggtInstance = null;
	private GGTThread ggtThread = null;
	ORB orb = null;

	Prozess rechts = null;
	Prozess links = null;
	Monitor monitor = null;
	public int verzoegerung = 0;
	private POA rootPoa;

	private String starterName = null;
	private int pid = 0;
	int currentNumber;
	private int startNumber;

	Koordinator koordinator = null;
	private boolean terminated = false;

	public ProzessImpl(ORB orb, POA rootPoa, String starterName, int id,
			Koordinator koordinator) {
		ggtThread = new GGTThread(this);
		ggtInstance = new Thread(ggtThread);

		this.starterName = starterName;
		this.orb = orb;
		this.rootPoa = rootPoa;
		this.pid = id;
		this.koordinator = koordinator;
	}

	@Override
	public void setParameters(Prozess rechts, Prozess links, int ggTStart,
			int verzoegerung, Monitor theMonitor) {
		this.links = links;
		this.rechts = rechts;
		this.monitor = theMonitor;
		this.verzoegerung = verzoegerung;
		this.currentNumber = ggTStart;
		this.startNumber = ggTStart;
		ggtInstance.start();
	}

	@Override
	public void terminate() {
		System.out.println("Terminiere Prozess...");
		if (terminated) {
			return;
		}
		
	
		terminated = true;
		ggtThread.terminate();
		ggtInstance.interrupt();
	

		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					ggtInstance.join();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			
				//rootPoa.destroy(true, true);
				orb.shutdown(true);
			}
		}).start();

	}

	@Override
	public String name() {

		return this.starterName + pid;

	}

	@Override
	public String starterName() {
		return this.starterName;
	}

	@Override
	public void fifoAddNumber(String fromName, int aNumber) {
		this.ggtThread.addNumber(fromName, aNumber);

	}

	@Override
	public void propagate() {
		System.out.println(this.name() + " propagiert " + this.currentNumber);
		this.links.fifoAddNumber(this.name(), this.currentNumber);
		this.rechts.fifoAddNumber(this.name(), this.currentNumber);
	}

	@Override
	public void fifoAddMarker(String from, int seqNo) {
		this.ggtThread.addMarker(from, seqNo);

	}

	@Override
	public void startSnapshot(int seqNo) {
		this.links.fifoAddMarker(this.name(), seqNo);
		this.rechts.fifoAddMarker(this.name(), seqNo);
	}

	

	@Override
	public int currentValue() {
		// TODO Auto-generated method stub
		return this.currentNumber;
	}

	@Override
	public int startValue() {
		// TODO Auto-generated method stub
		return this.startNumber;
	}


}
