package Prozess;

import Distributed_GCD.KoordinatorPackage.EProcessNotFound;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class GGTThread implements Runnable {

	abstract class Command {
		protected String senderName;

		Command(String senderName) {
			this.senderName = senderName;
		}

		public abstract void execute() throws EProcessNotFound;
	}

	class Rechnung extends Command {
		private final int newNumber;

		Rechnung(String name, int newNumber) {
			super(name);
			this.newNumber = newNumber;
		}

		@Override
		public void execute() {
			parent.monitor.rechnen(parent.name(), senderName, newNumber);
			if (newNumber < parent.currentNumber) {

				try {

					Thread.sleep(parent.verzoegerung);
					parent.currentNumber = ((parent.currentNumber - 1) % newNumber) + 1;

					markTerminate = false;
					System.out.println(parent.name() + "schlafe jetzt für "
							+ parent.verzoegerung + "ms, current time: "
							+ System.currentTimeMillis());

					parent.propagate();
					System.out.println(parent.name() + "time: "
							+ System.currentTimeMillis());

				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

		}
	}

	class TerminationRequest extends Command {
		private final int sequenceNumber;

		TerminationRequest(String name, int sequenceNumber) {
			super(name);
			this.sequenceNumber = sequenceNumber;
		}

		@Override
		public void execute() throws EProcessNotFound {

			if (sequenceNumber > previousSequenceNumber) {
				previousSequenceNumber = sequenceNumber;

				markTerminate = true;
				markRight = false;
				markLeft = false;

				parent.links.fifoAddMarker(parent.name(), sequenceNumber);
				parent.rechts.fifoAddMarker(parent.name(), sequenceNumber);
			}

			if (parent.links.name().equals(senderName)) {
				markLeft = true;
			} else if (parent.rechts.name().equals(senderName)) {
				markRight = true;
			}

			if (markRight && markLeft) {

				parent.koordinator.processStatus(markTerminate, parent.name(),
						sequenceNumber);

			}
			parent.monitor
					.terminieren(parent.name(), senderName, markTerminate);
			// System.out.println(parent.name() + " " + senderName +" "+
			// markTerminate);

		}
	}

	int previousSequenceNumber = -1;
	boolean markTerminate = true;
	boolean markRight = false;
	boolean markLeft = false;
	private ProzessImpl parent = null;
	public BlockingQueue<Command> cmdFifo = null;
	private volatile boolean running = true;

	public GGTThread(ProzessImpl parent) {
		super();
		cmdFifo = new LinkedBlockingQueue<Command>();
		this.parent = parent;
	}

	public void terminate() {
		running = false;
	}

	public void addMarker(String fromName, int seqNo) {
		this.cmdFifo.add(new TerminationRequest(fromName, seqNo));
	}

	@Override
	public void run() {
		while (running) {
			try {
				Command nextCommand = cmdFifo.take();
				nextCommand.execute();
			} catch (InterruptedException e) {
				System.out.println("Interrupted");
			} catch (Exception e) {

				e.printStackTrace();
			}
		}

		// this.parent.monitor.ergebnis(this.parent.name(),
		// this.parent.currentNumber);
	}

	public void addNumber(String fromName, int aNumber) {
		this.cmdFifo.add((new Rechnung(fromName, aNumber)));
	}
}
