package Koordinator;


import java.util.Properties;


import Distributed_GCD.Koordinator;
import Distributed_GCD.KoordinatorHelper;
import org.omg.CORBA.ORB;
import org.omg.CosNaming.NameComponent;
import org.omg.CosNaming.NamingContextExt;
import org.omg.CosNaming.NamingContextExtHelper;
import org.omg.PortableServer.POA;
import org.omg.PortableServer.POAHelper;

public class Main {

	public static void main(String[] args) {
		String name = args[0];

		try {
			Properties p = new Properties();
			p.put("org.omg.COBRA.ORBInitialPort", "1050");
			p.put("org.omg.COBRA.ORBInitialHost", "localhost");
			ORB orb = ORB.init(args, p);


			POA rootPoa = POAHelper.narrow(orb
					.resolve_initial_references("RootPOA"));

			rootPoa.the_POAManager().activate();
			final KoordinatorImpl servant = new KoordinatorImpl(orb, rootPoa, name);
			org.omg.CORBA.Object ref = rootPoa.servant_to_reference(servant);

			Koordinator href = KoordinatorHelper.narrow(ref);

			org.omg.CORBA.Object objRef = orb.resolve_initial_references("NameService");


			NamingContextExt ncRef = NamingContextExtHelper.narrow(objRef);

			NameComponent path[] = ncRef.to_name(name);
			ncRef.rebind(path, href);


			System.out.println("Koordinator ready and waiting");

			Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {

				@Override
				public void run() {
					servant.terminate();


				}

			}));
			orb.run();


			// e.printStackTrace();

		} catch (Exception e) {

			// System.err.println("ERROR: " + e);
			// e.printStackTrace(System.out);

		}
		System.out.println("server exiting");
	}
}
