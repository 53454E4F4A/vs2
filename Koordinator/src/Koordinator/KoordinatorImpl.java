package Koordinator;


import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicBoolean;

import monitor.Monitor;

import org.omg.CORBA.ORB;
import org.omg.PortableServer.POA;
import org.omg.PortableServer.POAPackage.ObjectNotActive;
import org.omg.PortableServer.POAPackage.ServantNotActive;
import org.omg.PortableServer.POAPackage.WrongPolicy;

import Distributed_GCD.KoordinatorPOA;
import Distributed_GCD.Prozess;
import Distributed_GCD.Starter;
import Distributed_GCD.StarterIdsHolder;
import Distributed_GCD.KoordinatorPackage.ENoStarterRegistered;
import Distributed_GCD.KoordinatorPackage.ENotReadyYet;
import Distributed_GCD.KoordinatorPackage.EProcessExists;
import Distributed_GCD.KoordinatorPackage.EStarterExists;
import Distributed_GCD.KoordinatorPackage.EStarterNotFound;


public class KoordinatorImpl extends KoordinatorPOA {

	final boolean debugging = true;
	final private String name;

	private ORB orb;
	private POA rootPoa;

	private ConcurrentHashMap<String, Prozess> prozesse;
	private ConcurrentHashMap<String, Starter> starter;

	private static final long MAX_PROCESS_WAIT = 30000;
	private HashMap<String, Integer> processesReady = null;


	private Monitor monitor = null;

	private static int seqNo = 0;


	public boolean ergebnisBereit = false;

	public List<Prozess> ring = null;

	public volatile boolean inRechnung = false;
	public volatile boolean KoordinatorRunning = true;
	public int exitTime = 0;


	public boolean terminated = false;

	private int ggT = 0;

	public  KoordinatorImpl(ORB o, POA rootPoa, String name) {
		this.orb = o;
		this.rootPoa = rootPoa;
		this.prozesse = new  ConcurrentHashMap<String, Prozess>();
		this.starter = new ConcurrentHashMap<String, Starter>();
		this.name = name;
	}

	@Override
	public void starterHinzufuegen(Starter theStarter,
			String name) throws EStarterExists {

		if (starter.containsKey(name)) {
			log("Starter existiert bereits: " + name);

			throw new EStarterExists("Name bereits vergeben");
		} else {

			starter.put(name, theStarter);

			log("Starter hinzugefügt: " + name);
		}
	}

	@Override
	public void starterEntfernen(String name) throws EStarterNotFound {
		if (!starter.containsKey(name)) {

			log("Starter existiert nicht: " + name);
			throw new EStarterNotFound("Starter existiert nicht");
		} else {
			starter.remove(name);
			log("Starter entfernt: " + name);
		}
	}

	@Override
	public void processHinzufuegen(Prozess theProcess,
			String name) throws EProcessExists {
	
		if (prozesse.containsKey(name)) {
			log("Process existiert bereits: " + name);
			throw new EProcessExists("Name bereits vergeben");
		} else {
			log("Adding process " + name);
			prozesse.put(name, theProcess);
		}
	}

	@Override
	public void starteRechnung(Monitor theMonitor, int minVerzoegerung,
			int maxVerzoegerung, int minProzesse, int maxProzesse, int exitTime,
			int ggT) throws ENoStarterRegistered, ENotReadyYet {

		if (this.inRechnung) {
			throw new ENotReadyYet("still calculating");
		} else if (this.starter.isEmpty()) {
			throw new ENoStarterRegistered("No starters Registered");
		}

		this.ggT = ggT;
		this.prozesse = new ConcurrentHashMap<String, Prozess>();
		this.monitor = theMonitor;
		this.exitTime = exitTime;
		this.inRechnung = true;

		int prozesseGesamt = 0;
		int before = prozesse.size();
		this.ergebnisBereit = false;

		if (this.starter.isEmpty()) {
			throw new ENoStarterRegistered("No starters Registered");
		}

		for (Starter s : this.starter.values()) {
			int anzahlProzesse = randomBetween(minProzesse, maxProzesse);
			s.starteProzesse(anzahlProzesse);
			prozesseGesamt += anzahlProzesse;
		}
		log("Erstelle " + prozesseGesamt + " Prozesse");
		warteAufProzesse(before + prozesseGesamt, MAX_PROCESS_WAIT);
		Prozess[] lowest = new Prozess[3];
		erstelleRing(minVerzoegerung, maxVerzoegerung, ggT, lowest);

		for (int i = 0; i < lowest.length; i++) {
			lowest[i].propagate();
			log("Starte mit " + lowest[i].name() + ": " + lowest[i].startValue());
		}

		class SnapShotThread implements Runnable {
			KoordinatorImpl koordinator = null;

			SnapShotThread(KoordinatorImpl koordinator) {
				this.koordinator = koordinator;
			}

			public void run() { 
				log("starte SnapshotThread " + this);
				while (!this.koordinator.ergebnisBereit && this.koordinator.inRechnung
						&& KoordinatorRunning) {

					try {
						Thread.sleep(this.koordinator.exitTime);
						if (!this.koordinator.ergebnisBereit && this.koordinator
								.inRechnung && KoordinatorRunning) {
							this.koordinator.startSnapshot();
						}
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						System.out.println("SnapShotThread Interrupted");
						//e.printStackTrace();
					}
				}
				log("beende SnapshotThread " + this);
			}
		}
		new Thread(new SnapShotThread(this)).start();
	}

	private void warteAufProzesse(int count, long max_wait) {
		long time_in_wait = 0;

		while ((this.prozesse.size() < count) && (max_wait > time_in_wait)) {

			try {
				time_in_wait += 1000;
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}


	private void erstelleRing(int minVerzoegerung, int maxVerzoegerung,
			int ggT, Prozess[] lowest) {

		ring = new LinkedList<Prozess>(this.prozesse.values());
		Collections.shuffle(ring, new Random(System.nanoTime()));

		Map<Prozess, Integer> prozessZuStartwertMap = new HashMap<>();

		for (int i = 0; i < ring.size(); i++) {
			int delay = randomBetween(minVerzoegerung, maxVerzoegerung);
			int ggTStart = randomMultipleOf(ggT);

			setParameters(ring, i, ggTStart, delay);
			prozessZuStartwertMap.put(ring.get(i), ggTStart);
		}

		if (this.monitor != null) {
			String PIDs[] = new String[ring.size()];
			int startValues[] = new int[ring.size()];

			int i = 0;
			for (Prozess p : ring) {
				startValues[i] = prozessZuStartwertMap.get(p);
				PIDs[i] = p.name();
				i++;
			}
			this.monitor.ring(PIDs);
			this.monitor.startzahlen(startValues);
		}

		for (int i = 0; i < lowest.length; i++) {
			lowest[i] = minValue(prozessZuStartwertMap);
			prozessZuStartwertMap.remove(lowest[i]);
		}


	}

	private int randomBetween(int minVerzoegerung, int maxVerzoegerung) {
		return minVerzoegerung + (int) (Math.random() * ((maxVerzoegerung -
				minVerzoegerung) + 1));
	}

	private int randomMultipleOf(int number) {
		return number * randomBetween(1, 100) * randomBetween(1, 100);
	}


	private void setParameters(List<Prozess> ring, int i, int ggTStart,
			int verzoegerung) {

		int leftNeighbourIndex = i == 0 ? ring.size() - 1 : i - 1;
		int rightNeighbourIndex = i == ring.size() - 1 ? 0 : i + 1;

		Prozess p = ring.get(i);
		p.setParameters(ring.get(rightNeighbourIndex), ring.get(leftNeighbourIndex),
				ggTStart, verzoegerung, monitor);
	}


	private <K> K minValue(Map<K, Integer> map) {
		Comparator<Map.Entry<K, Integer>> valueComparator = new Comparator<Map.Entry<K,
				Integer>>() {
			@Override
			public int compare(Map.Entry<K, Integer> o1, Map.Entry<K, Integer> o2) {
				return o1.getValue().compareTo(o2.getValue());
			}
		};

		return Collections.min(map.entrySet(), valueComparator).getKey();
	}

	@Override
	public void terminate() {

		if (this.terminated) {
			return;
		}

		KoordinatorRunning = false;

		
		for (Starter s : this.starter.values()) {
			s.terminate();
		}
		terminated = true;

		class shutdown implements Runnable {
			ORB orb;
			POA rootpoa;

			shutdown(ORB orb, POA rootPoa) {
				this.orb = orb;
				this.rootpoa = rootPoa;
			}

			public void run() {
				try {
					Thread.sleep(1000);
					this.orb.shutdown(true);
				} catch (InterruptedException e) {

					e.printStackTrace();
				}
				// this.rootpoa.destroy(true, true);

			}
		}
		 try {
			 rootPoa.deactivate_object(rootPoa.servant_to_id(this));
		 } catch (ObjectNotActive | WrongPolicy | ServantNotActive e) {

			 e.printStackTrace();
		}
		new Thread(new shutdown(this.orb, this.rootPoa)).start();
	}

	@SuppressWarnings("unused")

	private int getProcessReturnCode(String name) {
		return starter.get(prozesse.get(name).starterName()).getExitCode(name);
	}

	@Override

	public void processStatus(boolean finished, String Name, int seqNo) {
		int value = 0;
		//if(KoordinatorImpl.seqNo > seqNo) return;

		log("Snapshot " + seqNo + ": " + Name + (finished ? " bereit" : " nicht " +
				"bereit"));

		if(finished) this.processesReady.put(Name, seqNo);

		if (this.ring.size() == processesReady.size() && processesReady()) {
			ergebnisBereit = true;
			inRechnung = false;
			log("Ergebnis bereit");

			for (Prozess p : this.prozesse.values()) {
				try {
				
				    value = p.currentValue();
//					int startValue = p.startValue();
//
//
//					log(p.name() + ":\t" + "Wert: " + value + (value != this.ggT ? (" = " +
//							"" + this.ggT + " * " + (value / this.ggT)) + "t" : "\t") +
//							"= " + startValue + " / " + startValue / value);

					log("terminiere" + p.name());
					
					p.terminate();
					} catch (org.omg.CORBA.COMM_FAILURE e) {
						e.printStackTrace();
				}
			}
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			this.monitor.ergebnis(Name,  value);
		} else {
			
		}
		
	}

	private void startSnapshot() {

		KoordinatorImpl.seqNo++;
		int index = (int) (Math.random() * (this.prozesse.size() - 1));
		this.processesReady = new HashMap<String, Integer>();

		try {
			this.ring.get(index).fifoAddMarker("KoordinatorStarter", KoordinatorImpl.seqNo);
		} catch (org.omg.CORBA.COMM_FAILURE e) {

		}

		log("Starte Snapshot " + KoordinatorImpl.seqNo + "mit " + ring.get(index).name());
	}

	@Override
	public String name() {
		return this.name;
	}

	@Override
	public void getStarter(StarterIdsHolder starterListe) {

		starterListe.value = this.starter.keySet().toArray(new String[this.starter.size
				()]);
	}

	private boolean processesReady() {
		int seqNo;
		Collection<Integer> readySeqNos =  processesReady.values();
		seqNo = readySeqNos.iterator().next();
		for(int e :readySeqNos) {
			if(e != seqNo) return false;
		}
		return true;
	}


	private void log(String s) {
		if (debugging) {
			System.out.println(s);
		}
	}
}
