package Client;

import Distributed_GCD.Koordinator;
import Distributed_GCD.KoordinatorHelper;
import Distributed_GCD.KoordinatorPackage.ENoStarterRegistered;
import Distributed_GCD.KoordinatorPackage.ENotReadyYet;
import Distributed_GCD.StarterIdsHolder;
import monitor.Monitor;
import monitor.MonitorHelper;
import org.omg.CORBA.ORB;
import org.omg.CosNaming.NamingContextExt;
import org.omg.CosNaming.NamingContextExtHelper;


import java.util.Properties;



public class Main {

	public static void main(String[] args) {

		// TODO Auto-generated method stub
		ORB orb = null;
		NamingContextExt nc = null;

		String commandString = null;
		String koordinator = null;

		final String usage = "...";
		final String NameService = "NameService";
		try {
			
			commandString = args[0];
			koordinator = args[1];

			orb = ORB.init(args, new Properties());
			nc = NamingContextExtHelper.narrow(orb
					.resolve_initial_references(NameService));		

			org.omg.CORBA.Object obj = nc.resolve_str(koordinator);
			Koordinator krdntr = KoordinatorHelper.narrow(obj);

			switch (commandString) {

				case "liste":
					StarterIdsHolder StarterIdsHolder = new StarterIdsHolder();
					krdntr.getStarter(StarterIdsHolder);
					for (String s : StarterIdsHolder.value) {
						System.out.println(s);
					}
					break;
				case "rechne":

					String monitor = args[2];
					int minProzesse = Integer.parseInt(args[3]), maxProzesse = Integer
							.parseInt(args[4]);
					int minDelay = Integer.parseInt(args[5]),
							maxDelay = Integer.parseInt(args[6]);
					int exitTime = Integer.parseInt(args[7]);
					int ggT = Integer.parseInt(args[8]);

					obj = nc.resolve_str(monitor);

					Monitor mntr = MonitorHelper.narrow(obj);

					krdntr.starteRechnung(mntr, minDelay, maxDelay, minProzesse,
							maxProzesse, exitTime, ggT);
					break;
				case "terminate":
					krdntr.terminate();
					break;
				default:
					System.out.println("nope");
					break;
			}

			orb.destroy();
		} catch (org.omg.CORBA.COMM_FAILURE e ) {
			//System.out.println(e);
		} catch (ENoStarterRegistered e) {
			System.out.println("keine starter");
		} catch (ENotReadyYet e) {
			System.out.println("noch nicht bereit");
		} catch (org.omg.CORBA.UserException e) {
			System.out.println("UserException" + e);
		} catch (org.omg.CORBA.SystemException e) {
			System.out.println("SystemException" + e);
			//e.printStackTrace();
		} catch (NumberFormatException e) {
			System.out.println(e);
		} catch(ArrayIndexOutOfBoundsException e) {

			System.out.println(e);
		}
	}
}
