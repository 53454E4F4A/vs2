package Starter;


import java.util.Properties;

import Distributed_GCD.Koordinator;
import Distributed_GCD.KoordinatorHelper;
import Distributed_GCD.KoordinatorPackage.EStarterExists;
import Distributed_GCD.Starter;
import Distributed_GCD.StarterHelper;

import org.omg.CORBA.ORB;
import org.omg.CosNaming.NamingContextExt;
import org.omg.CosNaming.NamingContextExtHelper;
import org.omg.PortableServer.POA;
import org.omg.PortableServer.POAHelper;



public class Main {

	public static void main(String args[]) {
		final String name = args[0];
		String koordinator = args[1];
		String host  = args[2];
		String port = args[3];
		try {
		
	
			ORB orb = ORB.init(args, new Properties());


			POA rootPoa = POAHelper.narrow(orb.resolve_initial_references("RootPOA"));

			rootPoa.the_POAManager().activate();
			NamingContextExt nc = NamingContextExtHelper.narrow(orb
					.resolve_initial_references("NameService"));

			org.omg.CORBA.Object obj = nc.resolve_str(koordinator);
			final Koordinator k = KoordinatorHelper.narrow(obj);

			final StarterImpl starter = new StarterImpl(orb, rootPoa, name, k,
					host, Integer.parseInt(port));

			org.omg.CORBA.Object ref = rootPoa.servant_to_reference(starter);

			Starter href = StarterHelper.narrow(ref);
			Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {

				@Override
				public void run() {
					starter.terminate();
				}
			}));


			k.starterHinzufuegen(href, name);

			orb.run();
		} catch (EStarterExists e) {
			System.out.println("startername bereits vergeben");
		} catch (Exception e) {
			System.err.println("ERROR: " + e);
			// e.printStackTrace(System.out);
		}
		System.out.println("Starter Ende");
	}
}
