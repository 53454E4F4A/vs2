package Starter;


import Distributed_GCD.Koordinator;
import Distributed_GCD.KoordinatorPackage.EStarterNotFound;
import Distributed_GCD.StarterPOA;
import org.omg.CORBA.ORB;
import org.omg.PortableServer.POA;
import org.omg.PortableServer.POAPackage.ObjectNotActive;
import org.omg.PortableServer.POAPackage.ServantNotActive;
import org.omg.PortableServer.POAPackage.WrongPolicy;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

public class StarterImpl extends StarterPOA {

	private HashMap<String, Process> pmap = new HashMap<String, Process>();
	private static int PID = 0;
	private String name = null;
	private String java_home = null;
	private String class_path = null;
	private ORB orb = null;
	private POA rootPoa = null;
	private Koordinator koordinator;
	private String host;
	private int port;
	private boolean terminated = false;

	private final boolean debugging = true;

	public StarterImpl(ORB orb, POA rootPoa, String name, Koordinator koordinator,
			String host, int port) {

		java_home = System.getProperty("java.home") + "/bin/java";
		class_path = System.getProperty("java.class.path");
		this.rootPoa = rootPoa;
		this.orb = orb;
		this.name = name;
		this.koordinator = koordinator;
		this.host = host;
		this.port = port;
	}

	@Override
	public void starteProzesse(int count) {


		try {
			for (int i = 0; i < count; i++) {
				System.out.println("Starte Prozess " + name + PID);

				List<String> params = java.util.Arrays.asList("java", "-classpath",
						class_path + "../../Prozess/bin" + ":" + class_path + "../." +
								"./Koordinator/bin", "Prozess.Main", name, "" + PID,
						this.koordinator.name(), "-ORBInitialPort", "" + port,
						"-ORBInitialHost", host
				);
				
				ProcessBuilder b = new ProcessBuilder(params).inheritIO();
				PID++;
				pmap.put(name + PID, b.start());
			}
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

	}

	@Override
	public String name() {
		// TODO Auto-generated method stub
		return this.name;
	}

	@Override
	public int getExitCode(String name) {
		return this.pmap.get(name).exitValue();
	}



	@Override
	public void terminate() {
		if (terminated) {
			return;
		}
		terminated = true;
		log("Starte terminierung");
		try {
			koordinator.starterEntfernen(name);
			rootPoa.deactivate_object(rootPoa.servant_to_id(this));
		} catch (ObjectNotActive | WrongPolicy | ServantNotActive e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (EStarterNotFound e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


		beendeProzesse();
		log("Prozesse beendet");


		class shutdown implements Runnable {

			ORB orb;
			POA rootpoa;

			shutdown(ORB orb, POA rootPoa) {
				this.orb = orb;
				this.rootpoa = rootPoa;
			}

			public void run() {

				log("ShutdownThread Start");

				try {
					Thread.sleep(1000);
					this.orb.shutdown(true);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (org.omg.CORBA.BAD_INV_ORDER e) {
					//
				}

			}
		}
	
		new Thread(new shutdown(this.orb, this.rootPoa)).start();

	}

	@Override
	public void beendeProzesse() {
		boolean all_dead = false;
		
		
			all_dead = true;
			for (Process p : this.pmap.values()) {
				try {

					log(p + " exit code: " + p.exitValue() + ((p.exitValue() == 143) ?
							" (SIGTERM)" : ""));
				} catch (IllegalThreadStateException e) {
					p.destroy();
					all_dead = false;
					log("destroying: " + p);
				}

			}

			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
	}
	

	private void log(String s) {
		if (debugging) {
			System.out.println(s);
		}
	}
}
